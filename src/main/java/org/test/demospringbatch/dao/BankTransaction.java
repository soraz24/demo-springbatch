package org.test.demospringbatch.dao;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Transient;

import java.util.Date;
@Entity
@Data @AllArgsConstructor @NoArgsConstructor @ToString
public class BankTransaction {
    @Id
    private Long id;
    private Long accountID;
    private Date accountDate;
    @Transient
    private String strTransactionDate;
    private String transactionType;
    private double transactionAmount;

}
