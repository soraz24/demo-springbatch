package org.test.demospringbatch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.support.CompositeItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.transaction.PlatformTransactionManager;
import org.test.demospringbatch.dao.BankTransaction;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class SpringBatchConfig {

    @Bean
    public FlatFileItemReader<BankTransaction> fileItemReader() {
        Resource inputFile = new ClassPathResource("data.csv");
        FlatFileItemReader<BankTransaction> fileItemReader = new FlatFileItemReader<>();
        fileItemReader.setName("FFRI1");
        fileItemReader.setLinesToSkip(1);
        fileItemReader.setResource(inputFile);
        fileItemReader.setLineMapper(lineMapper());
        return fileItemReader;

    }

    @Bean
    public LineMapper<BankTransaction> lineMapper() {
        DefaultLineMapper<BankTransaction> lineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setDelimiter(",");
        lineTokenizer.setStrict(false);
        lineTokenizer.setNames("id", "accountID", "strTransactionDate", "transactionType", "transactionAmount");
        lineMapper.setLineTokenizer(lineTokenizer);
        BeanWrapperFieldSetMapper<BankTransaction> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(BankTransaction.class);
        lineMapper.setFieldSetMapper(fieldSetMapper);
        return lineMapper;
    }

    @Bean
    public BankTransactionItemProcessor processor() { return new BankTransactionItemProcessor();}

    @Bean
    public BankTransactionItemAnalyticsProcessor analyticsProcessor() { return new BankTransactionItemAnalyticsProcessor();}

    @Bean
    public BankTransactionItemWriter writer() { return new BankTransactionItemWriter();}

    @Bean
    public Job importTransactionJob(JobRepository jobRepository, Step bankStep) {
        Job job = new JobBuilder("importTransactionJob", jobRepository)
                .start(bankStep)
                .build();
        return job;
    }
    @Bean
    public Step bankStep(JobRepository jobRepository, PlatformTransactionManager transactionManager) {
        Step step1 = new StepBuilder("bankStep", jobRepository)
                .<BankTransaction, BankTransaction>chunk(100, transactionManager)
                .reader(fileItemReader())
                .processor(compositeItemProcessor())
                .writer(writer())
                .build();
        return step1;
    }

    @Bean
    public ItemProcessor<BankTransaction, BankTransaction> compositeItemProcessor() {
        List<ItemProcessor<BankTransaction, BankTransaction>> processors = new ArrayList<>();
        processors.add(processor());
        processors.add(analyticsProcessor());
        CompositeItemProcessor<BankTransaction, BankTransaction> compositeItemProcessor = new CompositeItemProcessor<>();
        compositeItemProcessor.setDelegates(processors);
        return compositeItemProcessor;
    }

}

