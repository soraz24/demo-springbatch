package org.test.demospringbatch;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;
import org.test.demospringbatch.dao.BankTransaction;

import java.text.SimpleDateFormat;

public class BankTransactionItemProcessor implements ItemProcessor<BankTransaction, BankTransaction> {
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy-HH:mm");
    @Override
    public BankTransaction process(BankTransaction bankTransaction) throws Exception {
        bankTransaction.setAccountDate(simpleDateFormat.parse(bankTransaction.getStrTransactionDate()));
        return bankTransaction;
    }
}
