package org.test.demospringbatch;

import lombok.Getter;
import org.springframework.batch.item.ItemProcessor;
import org.test.demospringbatch.dao.BankTransaction;

import java.text.SimpleDateFormat;

public class BankTransactionItemAnalyticsProcessor implements ItemProcessor<BankTransaction, BankTransaction> {
    @Getter private double totalDebit = 0;
    @Getter private double totalCredit = 0;
    @Override
    public BankTransaction process(BankTransaction bankTransaction) throws Exception {
        if(bankTransaction.getTransactionType().equals("D")) totalDebit += bankTransaction.getTransactionAmount();
        else if(bankTransaction.getTransactionType().equals("C")) totalCredit += bankTransaction.getTransactionAmount();
    return bankTransaction;
    }
}
